import React from 'react';
import { Authenticator } from 'aws-amplify-react';

import AuthenticatorContext from './authenticator-context';

import aws_exports from './aws-exports';

export default function StingyVeganAuthenticator(props) {
  const { children } = props;
  return (
    <Authenticator hideDefault={true} amplifyConfig={aws_exports}>
      <AuthenticatorToContext>{children}</AuthenticatorToContext>
    </Authenticator>
  );
}

function AuthenticatorToContext(props) {
  const { authData, authState, onAuthEvent, onStateChange, children } = props;
  const value = {
    authData,
    authState,
    onAuthEvent,
    onStateChange,
  };
  return (
    <AuthenticatorContext.Provider value={value}>
      {children}
    </AuthenticatorContext.Provider>
  );
}

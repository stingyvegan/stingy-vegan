import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
// import logger from 'redux-logger';
import createRootReducer from './redux/root';

export default function configureStore(history) {
  // const middlewares = [logger];
  const middlewares = [];
  const middlewareEnhancer = applyMiddleware(...middlewares);
  const composedEnhancers = composeWithDevTools(middlewareEnhancer);

  const store = createStore(createRootReducer(history), composedEnhancers);

  return store;
}

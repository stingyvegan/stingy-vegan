import { getRoles } from '../helpers/cognito';

const canAccessAdmin = (authData, authState) => {
  const roles = getRoles(authData);
  return roles.some(role => role.key === 'admin');
};

export default canAccessAdmin;

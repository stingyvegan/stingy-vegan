import { useState, useEffect } from 'react';

import * as productService from '../controllers/productController';
import * as subscriptionService from '../controllers/subscriptionService';

async function getProducts(filters, setProducts, setError, setLoading) {
  setError(undefined);
  setLoading(true);
  setProducts([]);
  try {
    if (filters.productId) {
      const product = await productService.fetchProduct(filters.productId);
      setProducts([product]);
    } else {
      const products = await productService.fetchProducts(filters);
      setProducts(products);
    }
  } catch (e) {
    setError(e);
  }
  setLoading(false);
}

export default function useProducts(filters) {
  const [products, setProducts] = useState([]);
  const [error, setError] = useState(undefined);
  const [loading, setLoading] = useState(true);

  const { active = true, productId } = filters;

  function handleProductEvent(event) {
    setProducts(existing =>
      existing.map(p => {
        if (p.productId === event.product.productId) {
          return event.product;
        }
        return p;
      }),
    );
  }

  async function reload() {
    await getProducts(filters, setProducts, setError, setLoading);
    subscriptionService.subscribe('PRODUCT_CHANGED', handleProductEvent);
  }

  useEffect(() => {
    reload();
    function cleanUp() {
      subscriptionService.unsubscribe('PRODUCT_CHANGED', handleProductEvent);
    }
    return cleanUp;
  }, [active, productId]);

  return [products, error, loading, reload];
}

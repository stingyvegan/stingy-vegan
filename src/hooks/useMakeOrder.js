import { useState } from 'react';

import * as orderService from '../controllers/orderController';

async function _makeOrder(
  orderId,
  productId,
  username,
  batchOrders,
  setIsLoading,
  setError,
) {
  let success = false;
  setIsLoading(true);
  try {
    await orderService.upsertOrder(orderId, productId, username, batchOrders);
    setError(undefined);
    success = true;
  } catch (e) {
    setError('Failed to make order');
  }
  setIsLoading(false);
  return success;
}

export default function useMakeOrder() {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(undefined);

  async function makeOrder(orderId, productId, username, batchOrders) {
    return _makeOrder(
      orderId,
      productId,
      username,
      batchOrders,
      setIsLoading,
      setError,
    );
  }

  function clearError() {
    setError(undefined);
  }

  return [isLoading, error, makeOrder, clearError];
}

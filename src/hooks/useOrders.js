import { useState, useEffect } from 'react';

import * as orderService from '../controllers/orderController';
import * as productService from '../controllers/productController';

async function getOrders(
  filters,
  setIsLoading,
  setOrders,
  setProducts,
  setIsLoaded,
  setError,
) {
  setIsLoading(true);
  setIsLoaded(false);
  setOrders([]);
  setProducts([]);
  try {
    const fetcher = filters.my
      ? orderService.fetchMyOrders()
      : orderService.fetchActiveOrders();
    const orders = await fetcher;
    const productIds = Object.keys(
      orders.reduce((products, order) => {
        return order.batchOrders.reduce((p, bo) => {
          return {
            ...p,
            [bo.batch.productId]: true,
          };
        }, products);
      }, {}),
    );
    const products = await Promise.all(
      productIds.map(pid => productService.fetchProduct(pid)),
    );
    setProducts(products);
    setOrders(orders);
    setError(undefined);
  } catch (err) {
    setError(err);
  } finally {
    setIsLoading(false);
    setIsLoaded(true);
  }
}

export default function useOrders(filters) {
  const [orders, setOrders] = useState([]);
  const [products, setProducts] = useState([]);
  const [isLoaded, setIsLoaded] = useState(false);
  const [error, setError] = useState(undefined);
  const [isLoading, setIsLoading] = useState(false);

  const { my } = filters;

  function reload() {
    getOrders(
      filters,
      setIsLoading,
      setOrders,
      setProducts,
      setIsLoaded,
      setError,
    );
  }

  useEffect(() => {
    reload();
  }, [my]);

  return [orders, products, isLoading, isLoaded, error, reload];
}

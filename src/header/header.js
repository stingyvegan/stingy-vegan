import React from 'react';
import { Menu, Dropdown } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import { Auth } from 'aws-amplify';

import {
  canAccessProducts,
  canAccessMyOrders,
  canAccessAdmin,
} from '../access';
import AuthenticatorContext from '../authenticator-context';

function LoginMenuItem({ authData, authState, onAuthEvent, onStateChange }) {
  const logout = async () => {
    try {
      await Auth.signOut();
      onStateChange('signedOut');
      onAuthEvent('signedOut', 'stateChange');
    } catch (err) {
      console.error(err);
    }
  };

  if (authState === 'signedIn') {
    return (
      <Menu.Menu position="right">
        <Dropdown item text={authData.attributes.name}>
          <Dropdown.Menu>
            <Dropdown.Item as={Link} to="/profile">
              My Profile
            </Dropdown.Item>
            <Dropdown.Item onClick={logout}>Logout</Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
      </Menu.Menu>
    );
  } else {
    return (
      <Menu.Item position="right" as={Link} to="/profile">
        Sign In
      </Menu.Item>
    );
  }
}

function Header({ loading }) {
  return (
    <AuthenticatorContext.Consumer>
      {({ authData, authState, onAuthEvent, onStateChange }) => (
        <Menu inverted stackable attached>
          {canAccessProducts(authData, authState) && (
            <Menu.Item as={Link} to="/products">
              Products
            </Menu.Item>
          )}
          {canAccessMyOrders(authData, authState) && (
            <Menu.Item as={Link} to="/orders">
              Orders
            </Menu.Item>
          )}
          {canAccessAdmin(authData, authState) && (
            <Menu.Item as={Link} to="/admin">
              Admin
            </Menu.Item>
          )}
          {!loading && (
            <LoginMenuItem
              authData={authData}
              authState={authState}
              onAuthEvent={onAuthEvent}
              onStateChange={onStateChange}
            />
          )}
        </Menu>
      )}
    </AuthenticatorContext.Consumer>
  );
}
export default Header;

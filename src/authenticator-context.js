import React from 'react';

const AuthenticatorContext = React.createContext({
  authData: undefined,
  authState: undefined,
  onAuthEvent: undefined,
  onStateChange: undefined,
});
export default AuthenticatorContext;

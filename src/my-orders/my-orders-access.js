const canAccessUserDashboard = (authData, authState) => {
  return authState === 'signedIn';
};

export default canAccessUserDashboard;

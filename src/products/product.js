import React, { Fragment, useState } from 'react';
import { Link } from 'react-router-dom';
import uuidv4 from 'uuid/v4';
import {
  Button,
  Loader,
  Header,
  Message,
  Grid,
  Image,
} from 'semantic-ui-react';

import AuthenticatorContext from '../authenticator-context';

import useProducts from '../hooks/useProducts';
import useMakeOrder from '../hooks/useMakeOrder';
import OrderEditor from './order-editor';

export default function Product(props) {
  const { match } = props;
  const { params } = match;

  const [products, error, loading, reload] = useProducts({
    productId: params.productId,
    active: false,
  });
  const product = products.length === 1 ? products[0] : undefined;
  const [isComplete, setIsComplete] = useState(false);

  const [orderLoading, orderError, makeOrder] = useMakeOrder();
  return (
    <AuthenticatorContext.Consumer>
      {({ authData }) => {
        const onCompleteCommit = async (productId, orderId, batchOrders) => {
          let filledOrderId = orderId ? orderId : uuidv4();
          const success = await makeOrder(
            filledOrderId,
            productId,
            authData.username,
            batchOrders,
          );
          if (success) {
            setIsComplete(true);
          }
        };
        return (
          <div>
            <Loader active={loading} />
            {error && (
              <Message negative>
                <Message.Header>Error Loading Product</Message.Header>
                <p>
                  Sorry, something went wrong. Press retry to attempt to load it
                  again.
                </p>
                <Button onClick={reload} color="red">
                  Retry
                </Button>
              </Message>
            )}
            {!loading && !error && (
              <Fragment>
                <Grid stackable>
                  <Grid.Row columns={2}>
                    <Grid.Column textAlign="center">
                      <Header as="h3">{product.name}</Header>
                      <Image src="/assets/img-placeholder-256.png" centered />
                    </Grid.Column>
                    <Grid.Column textAlign="center">
                      <Header as="h3">Order</Header>
                      {!isComplete ? (
                        <OrderEditor
                          product={product}
                          onCompleteCommit={onCompleteCommit}
                          loading={orderLoading}
                          error={orderError}
                        />
                      ) : (
                        <div>
                          <Message success>
                            Congratulations, your order was successful
                          </Message>
                          <Button.Group vertical>
                            <Button as={Link} to="/products">
                              Return To Products
                            </Button>
                            <Button as={Link} to="/orders">
                              View My Orders
                            </Button>
                            <Button onClick={() => setIsComplete(false)}>
                              Order More
                            </Button>
                          </Button.Group>
                        </div>
                      )}
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </Fragment>
            )}
          </div>
        );
      }}
    </AuthenticatorContext.Consumer>
  );
}

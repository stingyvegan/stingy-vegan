const canAccessProducts = (authData, authState) => {
  return authState === 'signedIn';
};

export default canAccessProducts;

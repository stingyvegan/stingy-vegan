import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';

import { trackPageView } from './controllers/analyticsController';

class GoogleAnalytics extends React.Component {
  constructor(props) {
    super(props);
    this.initialFired = false;
    this.lastUserType = undefined;
  }

  componentWillUpdate({ location, history }) {
    // Fire config for initial page load even if criteria not met.
    if (!this.initialFired) {
      this.initialFired = true;
      trackPageView(location);
    }

    if (location.pathname === this.props.location.pathname) {
      // don't log identical link clicks (nav links likely)
      return;
    }

    if (history.action === 'PUSH') {
      trackPageView(location);
    }
  }

  render() {
    return null;
  }
}

GoogleAnalytics.propTypes = {
  location: PropTypes.object.isRequired,
};

export default withRouter(GoogleAnalytics);

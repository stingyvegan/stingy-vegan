import React, { Fragment } from 'react';
import { Container, Loader } from 'semantic-ui-react';
import { Route } from 'react-router';

import AuthenticatorContext from './authenticator-context';

import requiresAuthorisation from './helpers/requires-authorisation';
import { canAccessMyOrders, canAccessProducts, canAccessAdmin } from './access';
import Header from './header/header';
import MyOrders from './my-orders/my-orders';
import Products from './products/products';
import Product from './products/product';
import Admin from './admin/admin';
import Profile from './login/profile';
import GoogleAnalytics from './google-analytics';
import './App.css';

function App() {
  return (
    <AuthenticatorContext.Consumer>
      {({ authData, authState }) => {
        const initialLoad = !authState || authState === 'loading';
        return (
          <Fragment>
            <Loader active={initialLoad}>Initialising</Loader>
            <Header loading={initialLoad} />
            <Container className="page">
              <Route
                path="/orders"
                exact
                component={requiresAuthorisation(
                  MyOrders,
                  canAccessMyOrders(authData, authState),
                  initialLoad,
                )}
              />
              <Route
                path="/products"
                exact
                component={requiresAuthorisation(
                  Products,
                  canAccessProducts(authData, authState),
                  initialLoad,
                )}
              />
              <Route
                path="/product/:productId"
                exact
                component={requiresAuthorisation(
                  Product,
                  canAccessProducts(authData, authState),
                  initialLoad,
                )}
              />
              <Route
                path="/admin"
                exact
                component={requiresAuthorisation(
                  Admin,
                  canAccessAdmin(authData, authState),
                  initialLoad,
                )}
              />
              <Route path="(/|/profile)" exact component={Profile} />
              <GoogleAnalytics />
            </Container>
          </Fragment>
        );
      }}
    </AuthenticatorContext.Consumer>
  );
}

export default App;

import React from 'react';
import { PageHeading } from './common';

function Unauthorised() {
  return <PageHeading icon="stop">Unauthorised</PageHeading>;
}
export default Unauthorised;

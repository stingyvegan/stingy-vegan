import React from 'react';
import { Header, Icon } from 'semantic-ui-react';

function PageHeading(props) {
  const { icon, children } = props;
  return (
    <Header as="h2" textAlign="center">
      {icon && <Icon name={icon} circular />}
      {children}
    </Header>
  );
}
export default PageHeading;

import { Observable } from 'rxjs';
import { Auth } from 'aws-amplify';

const wsUrl = `${process.env.REACT_APP_WS_HOST}:${
  process.env.REACT_APP_WS_PORT
}/api`;

let websocketObservable = undefined;

function connect(session, route) {
  return new Promise(resolve => {
    let ws = new WebSocket(
      `${wsUrl}${route}?Authorization=${encodeURI(
        `Bearer ${session.getAccessToken().getJwtToken()}`,
      )}`,
    );
    const interval = setInterval(() => {
      if (ws.readyState !== WebSocket.CONNECTING) {
        clearInterval(interval);
        resolve(ws);
      }
    }, 50);
  });
}

function tryConnect(session, route, attempts) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      connect(
        session,
        route,
      ).then(ws => {
        if (ws.readyState !== WebSocket.OPEN) {
          reject();
        } else {
          resolve(ws);
          attempts = 0;
        }
      });
    }, Math.min(Math.pow(attempts, 2), 10) * 1000);
  });
}

async function createWebSocket() {
  const session = await Auth.currentSession();

  let attempts = 0;
  let websocket;
  while (!websocket || websocket.readyState !== WebSocket.OPEN) {
    try {
      if (attempts > 0)
        console.log(`Websocket connection attempt #${attempts + 1}`);
      websocket = await tryConnect(session, '/ws', attempts);
    } catch (e) {}
    attempts += 1;
  }
  if (attempts > 1) console.log('Websocket connection successful');

  websocket.onclose = function() {
    console.log('Websocket connection lost');
    initialiseWebSocketObservable();
  };
  return websocket;
}

function initialiseWebSocketObservable() {
  websocketObservable = Observable.create(observer => {
    const onMessage = function(event) {
      const parsed = JSON.parse(event.data);
      observer.next(parsed);
    };
    const reconnect = function() {
      createWebSocket()
        .then(websocket => {
          websocket.onmessage = onMessage;
          websocket.onclose = reconnect;
        })
        .catch(err => observer.error(err));
    };
    reconnect();
  });
}

export function getWebSocketObservable() {
  if (!websocketObservable) {
    initialiseWebSocketObservable();
  }
  return websocketObservable;
}

import getAxios from './apiAxios';

async function fetchProduct(productId) {
  const axios = await getAxios();
  const result = await axios.get(`/products/${productId}`);
  return result.data;
}

async function fetchProducts() {
  const axios = await getAxios();
  const result = await axios.get('/products');
  return result.data;
}

export { fetchProducts, fetchProduct };

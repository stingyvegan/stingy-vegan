import { filter } from 'rxjs/operators';

import { getWebSocketObservable } from './wsController';

const currentSubscriptions = {};

function eventSubscription(event) {
  if (!currentSubscriptions[event]) {
    currentSubscriptions[event] = [];
  }
  return currentSubscriptions[event];
}

export function subscribe(eventType, handler) {
  const obs = getWebSocketObservable();
  const subscriber = obs
    .pipe(filter(event => event.type === eventType))
    .subscribe(handler);
  const eSub = eventSubscription(eventType);
  eSub.push({
    handler,
    subscriber,
  });
}

export function unsubscribe(eventType, handler) {
  const eSub = eventSubscription(eventType);
  const record = eSub.findIndex(entry => entry.handler === handler);
  if (record >= 0) {
    eSub[record].subscriber.unsubscribe();
    eSub.splice(record, 1);
    return true;
  }
  return false;
}

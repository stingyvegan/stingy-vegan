/**
 * Track a page view with google analytics, uses the document, window Global DOM objects.
 * @param {object} location Location object to use for path (from react router).
 */
export const trackPageView = location => {
  const { gtag } = window;
  if (!typeof gtag === 'function') return;
  gtag('config', process.env.REACT_APP_GOOGLE_ANALYTICS_ID, {
    page_title: document.title,
    page_location: window.location.href,
    page_path: location.pathname,
    custom_map: { dimension1: 'user_type' },
  });
};

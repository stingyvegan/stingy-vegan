import getAxios from './apiAxios';

async function fetchMyOrders() {
  const axios = await getAxios();
  const orders = await axios.get('/orders/my');
  return orders.data;
}

async function fetchActiveOrders() {
  const axios = await getAxios();
  const orders = await axios.get('/orders');
  return orders.data;
}

async function upsertOrder(orderId, productId, username, batchOrders) {
  const axios = await getAxios();
  const result = await axios.put('/orders', {
    orderId,
    productId,
    username,
    batchOrders,
  });
  return result.data;
}

export { fetchMyOrders, fetchActiveOrders, upsertOrder };

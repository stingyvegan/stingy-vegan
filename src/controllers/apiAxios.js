import axios from 'axios';
import { Auth } from 'aws-amplify';

const baseURL = `${process.env.REACT_APP_API_HOST}:${
  process.env.REACT_APP_API_PORT
}/api`;

async function getAxios(auth) {
  const session = await Auth.currentSession();
  const configured = axios.create({
    baseURL,
    headers: {
      authorization: `Bearer ${session.getAccessToken().getJwtToken()}`,
    },
  });
  return configured;
}

export default getAxios;

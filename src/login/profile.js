import React, { Fragment, useState } from 'react';
import { Auth } from 'aws-amplify';
import { Loader, Message } from 'semantic-ui-react';

import AuthenticatorContext from '../authenticator-context';

import { getRoles, getName } from '../helpers/cognito';

import Login from './login';
import CompleteAccount from './complete-account';
import Account from './account';

function Profile() {
  const [error, setError] = useState(undefined);
  const [loading, setLoading] = useState(false);
  return (
    <AuthenticatorContext.Consumer>
      {({ authData, authState, onAuthEvent, onStateChange }) => {
        const triggerAuthEvent = event => {
          const state = authState;
          if (onAuthEvent) {
            onAuthEvent(state, event);
          }
        };
        const changeState = (state, data) => {
          if (onStateChange) {
            onStateChange(state, data);
          }
          triggerAuthEvent({
            type: 'stateChange',
            data: state,
          });
        };
        const onSignIn = async (email, password) => {
          const sanitisedEmail = email.toLowerCase();
          setLoading(true);
          try {
            const user = await Auth.signIn(sanitisedEmail, password);
            if (user.challengeName === 'NEW_PASSWORD_REQUIRED') {
              changeState('requireNewPassword', user);
              setError(undefined);
            } else {
              const details = await Auth.currentUserInfo();
              changeState('signedIn', {
                ...user,
                attributes: details.attributes,
              });
              setError(undefined);
            }
          } catch (err) {
            if (err.code === 'UserNotConfirmedException') {
              changeState('confirmSignUp', { sanitisedEmail });
            } else if (err.code === 'PasswordResetRequiredException') {
              changeState('forgotPassword', { sanitisedEmail });
            } else {
              setError(err.message ? err.message : err);
            }
          } finally {
            setLoading(false);
          }
        };
        const onCompleteAccount = async (name, password) => {
          const user = authData;
          setLoading(true);
          try {
            const result = await Auth.completeNewPassword(user, password, {
              name,
            });
            const details = await Auth.currentUserInfo();
            changeState('signedIn', {
              ...result,
              attributes: details.attributes,
            });
            setError(undefined);
          } catch (err) {
            setError(err.message ? err.message : err);
          } finally {
            setLoading(false);
          }
        };
        let signInComponent = <Login loading={loading} onSignIn={onSignIn} />;
        switch (authState) {
          case 'signIn':
            break;
          case 'requireNewPassword':
            signInComponent = (
              <CompleteAccount
                loading={loading}
                onCompleteAccount={onCompleteAccount}
              />
            );
            break;
          case 'signedIn':
            signInComponent = (
              <Account
                loading={loading}
                name={getName(authData)}
                roles={getRoles(authData)}
              />
            );
            break;
          case 'loading':
            signInComponent = <Loader />;
            break;
          default:
            console.error(`unknown state: ${authState}`);
        }
        return (
          <Fragment>
            {error && <Message negative>{error}</Message>}
            {signInComponent}
          </Fragment>
        );
      }}
    </AuthenticatorContext.Consumer>
  );
}

export default Profile;

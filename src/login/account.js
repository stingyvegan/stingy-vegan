import React from 'react';
import { Segment, Label, Icon } from 'semantic-ui-react';

import { PageHeading } from '../common/common';

function Login(props) {
  const { name, loading, roles } = props;
  return (
    <Segment loading={loading}>
      <PageHeading icon="user">{name}</PageHeading>
      <p>Welcome, {name}</p>
      {roles.length > 0 && (
        <div>
          {roles.map(role => (
            <Label key={role.key}>
              <Icon name={role.icon} />
              {role.name}
            </Label>
          ))}
        </div>
      )}
    </Segment>
  );
}
export default Login;

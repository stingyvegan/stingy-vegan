import React from 'react';
import { Form, Button, Segment } from 'semantic-ui-react';
import { Formik, Field } from 'formik';

import { PageHeading } from '../common/common';

function CompleteAccount(props) {
  const { onCompleteAccount, loading } = props;
  return (
    <Segment loading={loading}>
      <PageHeading icon="user">Complete Account</PageHeading>
      <Formik
        initialValues={{ oldPassword: '', newPassword: '' }}
        onSubmit={(values, actions) => {
          onCompleteAccount(values.name, values.password);
        }}
        render={props => (
          <Form onSubmit={props.handleSubmit}>
            <Form.Field>
              <label>Name</label>
              <Field type="text" name="name" />
            </Form.Field>
            <Form.Field>
              <label>New Password</label>
              <Field type="password" name="password" />
            </Form.Field>
            <Button type="submit">Complete Account</Button>
          </Form>
        )}
      />
    </Segment>
  );
}
export default CompleteAccount;

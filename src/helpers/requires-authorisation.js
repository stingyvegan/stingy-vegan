import { Unauthorised } from '../common/common';

const requiresAuthorisation = (inputComponent, canAccess, initialLoad) => {
  if (canAccess) {
    return inputComponent;
  } else if (!initialLoad) {
    return Unauthorised;
  }
  return () => false;
};

export default requiresAuthorisation;

export const getRoles = authData => {
  if (
    !authData ||
    !authData.signInUserSession ||
    !authData.signInUserSession.accessToken
  ) {
    return [];
  }
  const groups =
    authData.signInUserSession.accessToken.payload['cognito:groups'] || [];
  const groupsWithBasic = ['basic', ...groups];
  return groupsWithBasic.map(r => roleDetails(r));
};

export const getName = authData => {
  return authData.attributes.name;
};

export const roleDetails = role => {
  let details = {
    key: role,
  };
  switch (role) {
    case 'admin':
      return {
        ...details,
        icon: 'cog',
        name: 'Administrator',
      };
    case 'basic':
      return {
        ...details,
        icon: 'user',
        name: 'Basic User',
      };
    default:
      return {
        ...details,
        icon: 'question',
        name: `Unknown (${role})`,
      };
  }
};

import uuid from 'uuid/v4';

export function updateBatches(currentBatches, newAmount, requiredUnits) {
  let remaining = newAmount;
  const updatedBatchOrders = currentBatches
    .map(bo => {
      if (remaining === 0) {
        return {
          ...bo,
          committed: 0,
        };
      } else if (bo.committed <= remaining) {
        remaining -= bo.committed;
        if (bo.committed < requiredUnits - bo.existingCommitted) {
          const toUse = Math.min(
            requiredUnits - bo.existingCommitted - bo.committed,
            remaining,
          );
          remaining -= toUse;
          return {
            ...bo,
            committed: bo.committed + toUse,
          };
        }
        return bo;
      } else {
        const toUse = remaining;
        remaining = 0;
        return {
          ...bo,
          committed: toUse,
        };
      }
    })
    .filter(bo => bo.committed > 0 || bo.existingCommitted > 0);
  const newBatchOrders = [];
  while (remaining > 0) {
    const toUse = Math.min(remaining, requiredUnits);
    remaining -= toUse;
    newBatchOrders.push({
      batchId: uuid(),
      existingCommitted: 0,
      committed: toUse,
    });
  }
  const result = updatedBatchOrders.concat(newBatchOrders);
  if (result.length > 0) {
    return result;
  } else {
    return [
      {
        batchId: uuid(),
        existingCommitted: 0,
        committed: 0,
      },
    ];
  }
}

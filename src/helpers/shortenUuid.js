/**
 * Get a short version of the given UUID.
 * @param {string} uuid The UUID to shorten.
 */
export default function shortenUuid(uuid) {
  return uuid.substring(uuid.length - 8, uuid.length);
}

# Stingy Vegan Client

This project is currently using `create-react-app` unejected.

## Development

To get started developing perform the initialisation steps below and then simply run `npm start` to start the dev server.

### Initialisation

First run `npm ci`, then `amplify init` and follow the prompts to set up your amplify profile. After selecting an environment use `amplify env pull` and you should be ready to go!

## Build for Production

AWS Amplify is currently being used for CI/CD.
If you want to create a production build manually use `npm run build`.
